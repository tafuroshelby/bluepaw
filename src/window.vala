/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;
using Gtk;
using Adw;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/window.ui")]
    public sealed class Window : Adw.ApplicationWindow
    {
        [GtkChild]
        public unowned Adw.Bin page_controls { get; }

        [GtkChild]
        private unowned Gtk.PopoverMenuBar menubar;
        [GtkChild]
        private unowned Adw.TabView tab_view;

        public Window(Application app)
        {
            Object(
                application: app
            );

            ActionEntry[] actionEntries = {
                {"new-tab", new_tab_cb, null, null, null},
                {"close-tab", close_tab_cb, null, null, null},
                {"next-tab", next_tab_cb, null, null, null},
                {"prev-tab", prev_tab_cb, null, null, null},
                {"show-menubar", show_menubar_cb, null, "false", null}
            };

            this.menubar.menu_model = new Gtk.Builder.from_resource("/com/pixel96x/BluePaw/ui/menubar.ui").get_object("menubar") as MenuModel;

            if (Application.preferences.window.get_boolean("remember-size")) {
                int width, height;
                Application.state.window.get("size", "(ii)", out width, out height);

                if (width > 0 && height > 0)
                    this.set_default_size(width, height);

                if (Application.state.window.get_boolean("maximized"))
                    this.maximize();
            }

            new PageManager(this.tab_view, this.page_controls);

            this.add_action_entries(actionEntries, this);
            this.application.set_accels_for_action("win.new-tab", {"<ctl>t"});
            this.application.set_accels_for_action("win.close-tab", {"<ctl>w"});
            this.application.set_accels_for_action("win.next-tab", {"<ctl>Tab"});
            this.application.set_accels_for_action("win.prev-tab", {"<ctl><shift>ISO_Left_Tab"});
            this.application.set_accels_for_action("win.show-menubar", {"<ctl><shift>F2"});

            this.close_request.connect((window) => {
                app.prepare_shutdown();
                return true;
            });
        }

        private void new_tab_cb()
        {
            new PageManager(this.tab_view, this.page_controls);
        }

        private void close_tab_cb()
        {
            this.tab_view.close_page(this.tab_view.selected_page);

            if (this.tab_view.n_pages < 1)
                new PageManager(this.tab_view, this.page_controls);
        }

        private void next_tab_cb()
        {
            if (this.tab_view.n_pages > 1)
                this.tab_view.select_next_page();
        }

        private void prev_tab_cb()
        {
            if (this.tab_view.n_pages > 1)
                this.tab_view.select_previous_page();
        }

        private void show_menubar_cb(SimpleAction action, Variant? param)
        {
            bool active = action.get_state().get_boolean();

            action.set_state(new Variant.boolean(!active));
            this.menubar.visible = !active;
        }
    }
}
