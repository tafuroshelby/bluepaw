/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    public sealed class PageManager : Object
    {
        public Adw.TabView tab_view { get; protected construct set; }
        public unowned Adw.Bin page_controls_bin { get; construct; }

        private Gtk.Box page_controls_box;
        private Gtk.Button prev_page_btn;

        private IPage current_page;
        private Adw.TabPage current_tab_page;
        private Adw.Bin tab_page_dummy_child;

        public PageManager(Adw.TabView tab_view,
                           Adw.Bin page_controls_bin)
        {
            Object(
                page_controls_bin: page_controls_bin,
                tab_view: tab_view
            );

            this.page_controls_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            this.page_controls_box.add_css_class("linked");
            this.page_controls_bin.child = page_controls_box;

            this.prev_page_btn = new Gtk.Button();
            this.prev_page_btn.child = new Gtk.Image.from_icon_name("go-previous-symbolic");
            this.prev_page_btn.sensitive = false;
            this.prev_page_btn.tooltip_text = "Go back to list";
            this.page_controls_box.append(this.prev_page_btn);

            this.prev_page_btn.clicked.connect((button) => {
                var postPage = this.current_page as PagePostView;
                this.tab_page_dummy_child.child = this.current_page = postPage.prev_page;
                button.sensitive = false;
            });

            this.tab_page_dummy_child = new Adw.Bin();
            this.tab_page_dummy_child.child = this.current_page = new PageHomepage(this);

            this.current_tab_page = this.tab_view.selected_page = this.tab_view.append(this.tab_page_dummy_child);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;

            this.tab_page_dummy_child.notify["child"].connect((obj, param) => {
                this.current_page = this.tab_page_dummy_child.child as IPage;
                this.current_tab_page.icon = this.current_page.icon;
                this.current_tab_page.title = this.current_page.title;
            });

            this.tab_view.notify["selected-page"].connect((obj, param) => {
                if (this.tab_view.selected_page == this.current_tab_page)
                    this.page_controls_bin.child = this.page_controls_box;
            });
        }

        public PageManager.new_from_search_result(Adw.TabView tab_view,
                                                  Adw.Bin page_controls_bin,
                                                  Posts.Post[]? posts)
        {
            Object(
                page_controls_bin: page_controls_bin,
                tab_view: tab_view
            );

            this.page_controls_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            this.page_controls_box.add_css_class("linked");
            this.page_controls_bin.child = this.page_controls_box;

            this.prev_page_btn = new Gtk.Button();
            this.prev_page_btn.child = new Gtk.Image.from_icon_name("go-previous-symbolic");
            this.prev_page_btn.sensitive = false;
            this.prev_page_btn.tooltip_text = "Go back to list";
            this.page_controls_box.append(this.prev_page_btn);

            this.prev_page_btn.clicked.connect((button) => {
                var postPage = this.current_page as PagePostView;
                this.tab_page_dummy_child.child = this.current_page = postPage.prev_page;
                button.sensitive = false;
            });

            this.tab_page_dummy_child = new Adw.Bin();
            this.tab_page_dummy_child.child = this.current_page = new PageSearchResults(this, posts);

            this.current_tab_page = this.tab_view.selected_page = this.tab_view.append(this.tab_page_dummy_child);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;

            this.tab_page_dummy_child.notify["child"].connect((obj, param) => {
                this.current_page = this.tab_page_dummy_child.child as IPage;
                this.current_tab_page.icon = this.current_page.icon;
                this.current_tab_page.title = this.current_page.title;
            });

            this.tab_view.notify["selected-page"].connect((obj, param) => {
                if (this.tab_view.selected_page == this.current_tab_page)
                    this.page_controls_bin.child = this.page_controls_box;
            });
        }

        public PageManager.new_from_post(Adw.TabView tab_view,
                                         Adw.Bin page_controls_bin,
                                         Posts.Post post,
                                         bool open_new_card = false)
        {
            Object(
                page_controls_bin: page_controls_bin,
                tab_view: tab_view
            );

            this.page_controls_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            this.page_controls_box.add_css_class("linked");
            this.page_controls_bin.child = this.page_controls_box;

            this.prev_page_btn = new Gtk.Button();
            this.prev_page_btn.child = new Gtk.Image.from_icon_name("go-previous-symbolic");
            this.prev_page_btn.sensitive = false;
            this.prev_page_btn.tooltip_text = "Go back to list";
            this.page_controls_box.append(prev_page_btn);

            this.prev_page_btn.clicked.connect((button) => {
                var postPage = this.current_page as PagePostView;
                this.tab_page_dummy_child.child = this.current_page = postPage.prev_page;
                button.sensitive = false;
            });

            this.tab_page_dummy_child = new Adw.Bin();
            this.tab_page_dummy_child.child = this.current_page = new PagePostView(this, post, null);

            this.current_tab_page = tab_view.append(tab_page_dummy_child);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;

            if (open_new_card)
                this.tab_view.selected_page = this.current_tab_page;

            this.tab_page_dummy_child.notify["child"].connect((obj, param) => {
                this.current_page = this.tab_page_dummy_child.child as IPage;
                this.current_tab_page.icon = this.current_page.icon;
                this.current_tab_page.title = this.current_page.title;
            });

            this.tab_view.notify["selected-page"].connect((obj, param) => {
                if (this.tab_view.selected_page == this.current_tab_page)
                    this.page_controls_bin.child = this.page_controls_box;
            });
        }

        public void load_homepage()
        {
            this.tab_page_dummy_child.child = new PageHomepage(this);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;
        }

        public void load_search_page(Posts.Post[]? posts,
                                     string search_query = "")
        {
            this.tab_page_dummy_child.child = new PageSearchResults(this, posts, search_query);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;
        }

        public void load_post_page(Posts.Post post)
        {
            this.tab_page_dummy_child.child = new PagePostView(this, post, this.current_page);
            this.current_tab_page.icon = this.current_page.icon;
            this.current_tab_page.title = this.current_page.title;
            this.prev_page_btn.sensitive = true;
        }
    }
}
