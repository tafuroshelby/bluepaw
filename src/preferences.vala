/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    public sealed class Preferences : Object
    {
        public Settings general { get; protected construct; }
        public Settings window { get; protected construct; }

        private Adw.StyleManager style_manager;

        public Preferences()
        {
            Object(
                general: new Settings("com.pixel96x.BluePaw.preferences.general"),
                window: new Settings("com.pixel96x.BluePaw.preferences.window")
            );

            this.style_manager = Adw.StyleManager.get_default();

            // reading saved preferences
            switch (this.general.get_enum("theme")) {
                case (0):
                    this.style_manager.color_scheme = Adw.ColorScheme.DEFAULT;
                    break;
                case (1):
                    this.style_manager.color_scheme = Adw.ColorScheme.FORCE_LIGHT;
                    break;
                case (2):
                    this.style_manager.color_scheme = Adw.ColorScheme.FORCE_DARK;
                    break;
            }

            // setting up signals
            this.general.changed["theme"].connect((prefs, key) => {
                switch (this.general.get_enum("theme")) {
                    case (0):
                        this.style_manager.color_scheme = Adw.ColorScheme.DEFAULT;
                        break;
                    case (1):
                        this.style_manager.color_scheme = Adw.ColorScheme.FORCE_LIGHT;
                        break;
                    case (2):
                        this.style_manager.color_scheme = Adw.ColorScheme.FORCE_DARK;
                        break;
                }
            });
        }
    }
}
